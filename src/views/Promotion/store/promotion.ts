import { computed, nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '../types/Promotion'
import type { VForm } from 'vuetify/components'
import { useReceiptStore } from '@/views/Receipt/stores/receipt'
import { promotions } from '@/api/promotionApi'
export const usePromotionStore = defineStore('promotion', () => {
  const receiptStore = usePromotionStore()
  const headers = [
    { title: 'ID', key: 'id'},
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Description', key: 'description', sortable: false },
    { title: 'Discount', key: 'discount', sortable: false },
    { title: 'Start Date', key: 'actions', sortable: false },
    { title: 'End Date', key: 'actions', sortable: false },
    { title: 'Status', key: 'actions', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false }
  ]

  const promotion = ref<Promotion[]>(promotions)
  const defaultPromotion = ref<Promotion>({
    id: -1,
    name: '',
    description: '',
    discount: 0,
    startDate: '',
    endDate: '',
    status: []
  })

  const promotionItems = ref<Promotion[]>([])

  let editedIndex = -1
  let lastId = 7

  const dialogDelete = ref(false)
  const refForm = ref<VForm | null>(null)
  const initialPromotion: Promotion = {
    id: -1,
    name: '',
    description: '',
    discount: 0,
    startDate: '',
    endDate: '',
    status: []
  }

  const editedPromotion = ref<Promotion>(JSON.parse(JSON.stringify(initialPromotion)))

  const sortPromotion = computed(() =>
    promotion.value.slice().sort((a,b) => b.id - a.id)
  )

  function sortId(){
    sortPromotion
  }

  const dialog = ref(false) //add promotion
  const dialogs = ref(false) //edit promotion
  function showDialog() {
    dialog.value = true // add
  }
  function showDialogs() {
    dialogs.value = true // edit
  }

  function editItem(item: any) {
    editedIndex = promotionItems.value.indexOf(item)
    editedPromotion.value = Object.assign({}, item)
    // selectPromotion.value = item
    editedPromotion.value = item
    dialogs.value = true
  }

  // function deleteItem(item: any) {
  //     editedIndex = promotion.value.indexOf(item)
  //     editedPromotion.value = Object.assign({}, item)
  //     dialogDelete.value = true
  //     // editedIndex = -1
  // }

  // function closeDelete () {
  //     dialogDelete.value = false
  //     nextTick(() => {
  //         editedPromotion.value = Object.assign({}, initialPromotion)
  //     })
  // }
  function closeDelete() {
    dialogDelete.value = false

    // nextTick(() => {
    //     editedPromotion.value = Object.assign({}, initialPromotion)
    // })
  }

  function deleteItem(item: any) {
    editedIndex = promotion.value.indexOf(item)
    editedPromotion.value = Object.assign({}, item)
    dialogDelete.value = true
    // editedIndex = -1
  }

  function deleteItemConfirm() {
    promotion.value.splice(editedIndex, 1)
    closeDelete()
  }

  function closeDialog() {
    if (currentPromotion.value != null) {
      dialog.value = false
      dialogs.value = false
    }

    nextTick(() => {
      editedPromotion.value = Object.assign({}, initialPromotion)
      editedIndex = -1
    })
  }

  function clear() {
    promotionItems.value = []
    defaultPromotion.value = {
      id: 0,
      name: '',
      description: '',
      discount: 0,
      startDate: '',
      endDate: '',
      status: []
    }

    editedPromotion.value = defaultPromotion.value
    currentPromotion.value = null
    console.log(currentPromotion.value)

    // clear()
  }

  async function save() {
    // const { valid } = await refForm.value!.validate()
    // if(!valid) return
    if (editedIndex > -1) {
      Object.assign(promotion.value[editedIndex], editedPromotion.value)
      console.log(promotion)
    } else {
      editedPromotion.value.id = lastId++
      console.log(editedPromotion.value)
      promotion.value.push(editedPromotion.value)
      console.log(promotionItems.value)
      console.log(promotion.value)
    }
    // editedIndex = -1
    console.log(promotion.value)
    closeDialog()
    clear()
  }

  const currentPromotion = ref<Promotion | null>()
  function useSelectPromotion(selectPromotion: Promotion) {
    if (selectPromotion.status.includes('active')) {
      currentPromotion.value = selectPromotion
      console.log(currentPromotion.value.name)
    }

    // dialog.value = false
  }
  return {
    promotion,
    promotions,
    dialog,
    dialogs,
    editedIndex,
    editedPromotion,
    defaultPromotion,
    dialogDelete,
    currentPromotion,
    promotionItems,
    sortPromotion,
    headers,
    editItem,
    showDialog,
    showDialogs,
    deleteItem,
    deleteItemConfirm,
    closeDelete,
    closeDialog,
    clear,
    useSelectPromotion,
    save,
    sortId

  }
})

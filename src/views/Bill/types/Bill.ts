export type Bill = {
    id: number
    name: string
    date: Date
    worth: number
    status: boolean
}
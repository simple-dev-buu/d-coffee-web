import { computed, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Bill } from '../types/Bill'
import { defaultBill, billPaid, billPending } from '@/api/billApi'

export const useBillStore = defineStore('bill', () => {
  const bill = ref<Bill>(defaultBill)
  const billsPaid = ref<Bill[]>([...billPaid])
  const billsPending = ref<Bill[]>([...billPending])
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const addDialog = ref(false)
  const dialogPay = ref(false)
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Date', key: 'date', sortable: false },
    { title: 'Worth', key: 'worth', sortable: false },
    { title: 'Status', key: 'status', sortable: false }
  ]

  function showDialog() {
    addDialog.value = true
  }

  function showPay() {
    dialogPay.value = true
  }

  function closePay() {
    dialogPay.value = false
    editedIndex.value = -1
    editedItem.value = defaultBill
  }

  const editedIndex = ref(-1)

  function closeDelete() {
    dialogDelete.value = false
    editedIndex.value = -1
    editedItem.value = defaultBill
  }

  function deleteItemConfirm() {
    if (editedItem.value.status) {
      billsPaid.value.splice(editedIndex.value)
    } else {
      billsPending.value.splice(editedIndex.value)
    }
    closeDelete()
  }

  const editedItem = ref(defaultBill)
  function close() {
    dialog.value = false
    editedItem.value = Object.assign({}, defaultBill)
  }

  function save() {
    const lastId = billsPending.value.reduce(
      (maxId, user) => (user.id > maxId ? user.id : maxId),
      0
    )
    editedItem.value.id = lastId + 1
    billsPending.value.push(editedItem.value)
    close()
  }

  function confirmPay() {
    editedItem.value.status = true
    billsPending.value.splice(editedIndex.value)
    const lastId = billsPaid.value.reduce((maxId, user) => (user.id > maxId ? user.id : maxId), 0)
    editedItem.value.id = lastId + 1
    billsPaid.value.push(editedItem.value)
    closePay()
  }

  function deleteItem(item: Bill) {
    if (item.status) {
      editedIndex.value = billsPaid.value.indexOf(item)
      editedItem.value = Object.assign({}, item)
    } else {
      editedIndex.value = billsPending.value.indexOf(item)
      editedItem.value = Object.assign({}, item)
    }

    dialogDelete.value = true
  }

  function pay(item: Bill) {
    editedIndex.value = billsPending.value.indexOf(item)
    editedItem.value = Object.assign({}, item)
    showPay()
  }

  return {
    bill,
    billsPaid,
    billsPending,
    dialog,
    dialogDelete,
    headers,
    editedIndex,
    editedItem,
    addDialog,
    dialogPay,
    showPay,
    closePay,
    close,
    save,
    deleteItem,
    deleteItemConfirm,
    closeDelete,
    showDialog,
    confirmPay,
    pay
  }
})

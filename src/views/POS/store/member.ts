import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '../../Customer/types/Member'
import { customers } from '@/api/customerApi'
import { useReceiptStore } from '@/views/Receipt/stores/receipt'

export const useMemberStore = defineStore('member', () => {
  const memberDialog = ref(false)
  const members = ref<Member[]>(customers)
  const receiptStore = useReceiptStore()
  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if (index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
    receiptStore.receipt.member = currentMember.value
  }
  function clear() {
    currentMember.value = null
  }
  function showMemberDialog() {
    memberDialog.value = true
    // currentMember.value
    // receipt.value.receiptItem = receiptItems.value
  }

  return {
    members,
    currentMember,
    memberDialog,
    searchMember,
    clear,
    showMemberDialog
  }
})

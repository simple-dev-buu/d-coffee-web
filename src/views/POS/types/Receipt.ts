import type { Promotion } from '@/views/Promotion/types/Promotion'
import type { Member } from '../../Customer/types/Member'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type Receipt = {
  id: number
  createDate: Date
  totalUnit: number
  totalBefore: number
  discount: number
  netPrice: number
  receivedAmount: number
  change: number
  promotionId: number
  promotion?: Promotion
  paymentType: string
  userId: number
  user?: User
  memberId: number
  member?: Member
  givePoint: number
  usePoint: number
  totalPoint: number
  receiptItem?: ReceiptItem[]
}

export type { Receipt }

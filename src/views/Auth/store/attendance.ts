import { useChecktimesStore } from '@/views/Checktime/stores/checktime'
import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useAttendanceStore = defineStore('attendance', {
  state: () => {
    return {
      dialogState: ref(false),
      checkTimeStore: useChecktimesStore()
    }
  },
  actions: {
    openAttendanceDialog() {
      this.dialogState = true
    },
    closeDialog() {
      this.dialogState = false
    },
    clockIn() {
      this.checkTimeStore.actions.saveClockIn()
      this.closeDialog()
    },
    clockOut() {
      this.checkTimeStore.actions.saveClockOut()
      this.closeDialog()
    }
  }
})

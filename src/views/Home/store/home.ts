import { defineStore } from 'pinia'
import type { TopSaleItem } from '../types/topsales'
import { ref } from 'vue'
import type { LastTransactionItem } from '../types/transaction'

export const useHomeStore = defineStore('home', () => {
  const data = {
    topSales: ref(<TopSaleItem[]>[]),
    recentTransactions: ref(<LastTransactionItem[]>[])
  }

  const methods = {
    fetchTopSales() {
      data.topSales.value = [
        {
          product: { id: 1, name: 'เอสเปรสโซ', price: 50, type: 1 },
          amount: 10,
          totalValue: 10 * 50
        },
        {
          product: { id: 2, name: 'ชาเขียว', price: 50, type: 1 },
          amount: 10,
          totalValue: 10 * 50
        },
        {
          product: { id: 2, name: 'ชาเขียว', price: 50, type: 1 },
          amount: 10,
          totalValue: 10 * 50
        },
        {
          product: { id: 2, name: 'ชาเขียว', price: 50, type: 1 },
          amount: 10,
          totalValue: 10 * 50
        },
        {
          product: { id: 2, name: 'ชาเขียว', price: 50, type: 1 },
          amount: 10,
          totalValue: 10 * 50
        }
      ]
    },
    fetchRecentTransactions() {
      data.recentTransactions.value = [
        {
          id: 1,
          createDate: new Date(),
          total: 1000
        }
      ]
    }
  }

  return { data, methods }
})

import { computed, nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '../../POS/types/Products'
import ProductDialogVue from '../view/ProductDialog.vue'
import { appetizer, drinks, foods } from '@/api/productApi'

export const useProductsStore = defineStore('products', () => {
  const p_drinks = ref<Product[]>(drinks)
  const p_appetizer = ref<Product[]>(appetizer)
  const p_foods = ref<Product[]>(foods)

  const AllProduct = ref<Product[]>([
    { id: 1, name: 'เอสเปรสโซ', price: 50, type: 1 },
    { id: 2, name: 'ลาเต้', price: 50, type: 1 },
    { id: 3, name: 'คาปูชิโน่', price: 50, type: 1 },
    { id: 4, name: 'อเมริกาโน่', price: 50, type: 1 },
    { id: 5, name: 'ม็อคค่า', price: 50, type: 1 },
    { id: 6, name: 'มัคคิอาโต้', price: 50, type: 1 },
    { id: 7, name: 'แฟลตไวท์', price: 50, type: 1 },
    { id: 8, name: 'กาแฟดำ', price: 50, type: 1 },
    { id: 9, name: 'อาราบิก้า', price: 50, type: 1 },
    { id: 10, name: 'ชาขาว', price: 55, type: 1 },
    { id: 11, name: 'ชาเขียว', price: 60, type: 1 },
    { id: 12, name: 'ชาเหลือง', price: 50, type: 1 },
    { id: 13, name: 'ชาอู่หลง', price: 55, type: 1 },
    { id: 14, name: 'ชาแดง', price: 45, type: 1 },
    { id: 15, name: 'ชาดำ', price: 50, type: 1 },
    { id: 16, name: 'โอริโอ้ปั่น', price: 40, type: 1 },
    { id: 17, name: 'สตรอว์เบอร์รี่ปั่น', price: 40, type: 1 },
    { id: 18, name: 'ลิ้นจี่นมสดปั่น', price: 40, type: 1 },
    { id: 19, name: 'เบอร์รี่โยเกิร์ต', price: 45, type: 1 },
    { id: 20, name: 'สตรอว์เบอร์รี่โยเกิร์ต', price: 45, type: 1 },
    { id: 21, name: 'เค้กชิฟฟ่อนช็อกโกแลต', price: 60, type: 2 },
    { id: 22, name: 'แซนด์วิช', price: 35, type: 2 },
    { id: 23, name: 'บัตเตอร์เค้ก', price: 45, type: 2 },
    { id: 24, name: 'ขนมปัง', price: 25, type: 2 },
    { id: 25, name: 'ครัวซองต์', price: 35, type: 2 },
    { id: 26, name: 'คัพเค้ก', price: 30, type: 2 },
    { id: 27, name: 'คุกกี้', price: 20, type: 2 },
    { id: 28, name: 'เค้ก', price: 55, type: 2 },
    { id: 29, name: 'พายสับปะรด', price: 50, type: 2 },
    { id: 31, name: 'ข้าวผัด', price: 40, type: 3 },
    { id: 32, name: 'ข้าวไข่ข้นแฮม', price: 35, type: 3 },
    { id: 33, name: 'สปาเก็ตตี้คาโบนาร่า', price: 55, type: 3 },
    { id: 34, name: 'สปาเก็ตตี้ซอสมะเขือเทศ', price: 50, type: 3 },
    { id: 35, name: 'เบอร์เกอร์', price: 50, type: 3 },
    { id: 36, name: 'เฟรนซ์ฟราย', price: 35, type: 3 },
    { id: 37, name: 'ฮ็อตด็อก', price: 30, type: 3 },
    { id: 38, name: 'แพนเค้ก', price: 40, type: 3 },
    { id: 39, name: 'สลัด', price: 55, type: 3 }
  ])

  const productTypes = computed(() => {
    const typesSet = new Set(AllProduct.value.map((products) => products.type))
    return Array.from(typesSet)
  })

  const selectedTab = ref(0)

  const getProductsByType = (type: any) => {
    return AllProduct.value.filter((products) => products.type === type)
  }

  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Picture', key: 'picture', sortable: false },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Price', key: 'price', sortable: false },
    { title: 'Type', key: 'type', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false },
    
  ]

  let lastId = 40
  const currentProduct = ref<Product | null>()
  const searchProduct = (name: string) => {
    const index1 = AllProduct.value.findIndex((item) => item.name === name)
    // const index2 = products2.value.findIndex((item) => item.name === name)
    // const index3 = products3.value.findIndex((item) => item.name === name)
    if (index1 < 0) {
      currentProduct.value = null
    }
    currentProduct.value = AllProduct.value[index1]
  }

  const product = ref<Product>({
    id: -1,
    name: '',
    price: 0,
    type: 0
  })
  const initialProduct: Product = {
    id: -1,
    name: '',
    price: 0,
    type: 0
  }
  let editedIndex = -1
  const dialogDelete = ref(false)
  const dialog = ref(false) //add promotion
  const dialogs = ref(false) //edit promotion
  function showDialog() {
    dialog.value = true // add
  }
  function showDialogs() {
    dialogs.value = true // edit
  }

  const editedProduct = ref<Product>(JSON.parse(JSON.stringify(initialProduct)))
  const productItems = ref<Product[]>([])

  function editItem(item: any) {
    editedIndex = AllProduct.value.indexOf(item)
    editedProduct.value = Object.assign({}, item)
    product.value = item

    dialogs.value = true
    console.log(item)

    console.log(product.value)
    console.log(productItems.value)
  }

  //   function closeDelete () {
  //     dialogDelete.value = false
  //     nextTick(() => {
  //         editedProduct.value = Object.assign({}, initialProduct)
  //     })
  // }
  function closeDelete() {
    dialogDelete.value = false
  }

  // function deleteItemConfirm(productItem: Product) {
  //   const index = productItems.value.findIndex((item) => item === productItem)
  //   productItems.value.splice(index, 1)
  //   products1.value.splice(editedIndex,1)
  //   closeDelete()
  //   console.log(editedIndex)

  // }
  function deleteItemConfirm() {
    AllProduct.value.splice(editedIndex, 1)
    closeDelete()
  }

  function deleteItem(item: any) {
    editedIndex = AllProduct.value.indexOf(item)
    editedProduct.value = Object.assign({}, item)
    dialogDelete.value = true
    // editedIndex = -1
  }

  function clear() {
    productItems.value = []
    product.value = {
      id: 0,
      name: '',
      price: 0,
      type: 0
    }
    editedProduct.value = product.value
    // clear()
  }

  function closeDialog() {
    dialog.value = false
    dialogs.value = false
    nextTick(() => {
      editedProduct.value = Object.assign({}, initialProduct)
      editedIndex = -1
    })
  }

  async function save() {
    // const { valid } = await refForm.value!.validate()
    // if(!valid) return
    if (editedIndex > -1) {
      Object.assign(AllProduct.value[editedIndex], editedProduct.value)
      console.log(editedProduct)
    } else {
      editedProduct.value.id = lastId++
      console.log(editedProduct.value)
      AllProduct.value.push(editedProduct.value)
      console.log(editedProduct.value)
      console.log(AllProduct.value)
    }
    // editedIndex = -1
    console.log(productItems.value)
    closeDialog()
    clear()
  }

  return {
    AllProduct,
    p_drinks,
    p_appetizer,
    p_foods,
    currentProduct,
    editedIndex,
    dialog,
    dialogs,
    product,
    dialogDelete,
    editedProduct,
    productTypes,
    selectedTab,
    headers,
    searchProduct,
    editItem,
    deleteItem,
    clear,
    deleteItemConfirm,
    closeDelete,
    showDialog,
    showDialogs,
    save,
    getProductsByType
  }
})

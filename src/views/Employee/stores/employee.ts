import { ref, nextTick } from 'vue'
import { defineStore } from 'pinia'
import { type Employee } from '../types/Employee'
import type { empRole, empGender, empQualification } from '../types/Employee';
import { employees as employeesApi } from '@/api/employeeApi'

export const useEmployeeStore = defineStore('employees', () => {
  const employees = ref<Employee[]>(employeesApi)

  const employeeStore = useEmployeeStore()
  const getEmployees = () => {
    const employeesJson: string = JSON.stringify(employeeStore.employees)
    return employeesJson
  }

  const currentEmployee = ref<Employee | null>()

  const searchEmployee = (tel: string, firstName: string, lastName: string, birthDate: string, startDate:string) => {
    const index1 = employees.value.findIndex((item) => 
    item.firstName === firstName &&
      item.tel === tel &&
      item.lastName === lastName &&
      item.birthDate === birthDate &&
      item.startDate === startDate)
      if (index1 < 0) {
        currentEmployee.value = null;
      } else {
        currentEmployee.value = employees.value[index1];
      }
  }
  //const searchEmployee = (tel: string) => {
    //const index1 = employees.value.findIndex((item) => item.tel === tel)
    //if (index1 < 0) {
      //currentEmployee.value = null;
    //} else {
      //currentEmployee.value = employees.value[index1];
    //}
  //};

  const headers = [
    { title: 'ID', key: 'id'},
    { title: 'Role', key: 'role', sortable: false },
    { title: 'First Name', key: 'firstName', sortable: false },
    { title: 'Last Name', key: 'lastName', sortable: false },
    { title: 'Tel.', key: 'tel', sortable: false },
    { title: 'Gender', key: 'gender', sortable: false },
    { title: 'Birth Date', key: 'birthDate', sortable: false },
    { title: 'Qualification', key: 'qualification', sortable: false },
    { title: 'MoneyRate(baht.)/month', key: 'moneyrate', sortable: false },
    { title: 'MinWorkHour/month', key: 'minworkhour', sortable: false },
    { title: 'Start Date', key: 'startDate', sortable: false },
    { title: 'Action', key: 'action', sortable: false }
  ]
  

  const employee1 = ref<Employee>({
    id: -1,
    roles: '' as empRole,
    firstName: '',
    lastName: '',
    tel: '',
    gender: '' as empGender,
    birthDate: '',
    qualification: '' as empQualification,
    moneyrate: 0,
    minWorkingHour: 0,
    startDate: ''
  })
  const initialEmployee: Employee = {
    id: -1,
    roles: '' as empRole,
    firstName: '',
    lastName: '',
    tel: '',
    gender: '' as empGender,
    birthDate: '',
    qualification: '' as empQualification,
    moneyrate: 0,
    minWorkingHour: 0,
    startDate: ''
  }

  let lastId = 16
  let editedIndex = -1
  const dialogDel = ref(false)
  const dialogAdd = ref(false) //add 
  const dialogEdit = ref(false) //edit 


  function showDialogAdd() {
    dialogAdd.value = true 
  }
  function showDialogEdit() {
    dialogEdit.value = true 
  }
  const editedEmployee = ref<Employee>(JSON.parse(JSON.stringify(initialEmployee)))
  const employeeItems = ref<Employee[]>([])

  function editItem(item: any) {
    editedIndex = employees.value.indexOf(item)
    editedEmployee.value = Object.assign({}, item)
    employee1.value = item

    dialogEdit.value = true
  }

  
///
///
  function closeDelete() {
    dialogDel.value = false
}

///
///

  function deleteItem(item: any) {
    editedIndex = employees.value.indexOf(item)
    editedEmployee.value = Object.assign({}, item)
    dialogDel.value = true
    // editedIndex = -1
  }

  function deleteItemConfirm() {
    employees.value.splice(editedIndex, 1)
    closeDelete()
  }

  function clear() {
    employeeItems.value = []
    employee1.value = {
      id: 0,
      roles: '' as empRole,
      firstName: '',
      lastName: '',
      tel: '',
      gender: '' as empGender,
      birthDate: '',
      qualification: '' as empQualification,
      moneyrate: 0,
      minWorkingHour: 0,
      startDate: ''
    }
    editedEmployee.value = employee1.value
    // clear()
  }

  function closeDialog() {
    dialogAdd.value = false
    dialogEdit.value = false
    nextTick(() => {
      editedEmployee.value = Object.assign({}, initialEmployee)
      editedIndex = -1
    })
  }

  async function save() {
    // const { valid } = await refForm.value!.validate()
    // if(!valid) return
    if (editedIndex > -1) {
      Object.assign(employees.value[editedIndex], editedEmployee.value)
      console.log(employees)
    } else {
      editedEmployee.value.id = lastId++
      console.log(editedEmployee.value)
      employees.value.push(editedEmployee.value)
      console.log(editedEmployee.value)
      console.log(employees.value)
    }
    // editedIndex = -1
    console.log(employeeItems.value)
    closeDialog()
    clear()
  }



  return {
    employees, currentEmployee, editedIndex, dialogAdd, dialogEdit, employee1, dialogDel, editedEmployee, employeeItems, headers,
    searchEmployee, editItem, deleteItem, clear, deleteItemConfirm, showDialogAdd, showDialogEdit, save , closeDialog , closeDelete
    
  }

 // return { 
  // members, currentMember, editedIndex, dialog, dialogs, member, dialogDelete,editedMember, 
   //   searchMember, editItem, deleteItem , clear, deleteItemConfirm, closeDelete, showDialog, showDialogs, save 
 // }
})

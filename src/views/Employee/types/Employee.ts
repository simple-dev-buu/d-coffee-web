type empGender = 'ผู้ชาย' | 'ผู้หญิง'
type empRole = 'ผู้จัดการ' | 'พนักงาน' | 'เจ้าของร้าน'
type empQualification = 'ปริญญาตรี' | 'ปริญญาโท' | 'ปวส.' | 'ปวช.'
type Employee = {
  id: number
  //branch
  roles: empRole
  firstName: string
  lastName: string
  tel: string
  gender: empGender
  birthDate: string
  qualification: empQualification
  moneyrate: number
  minWorkingHour: number
  startDate: string
}

export function getFullName(employee: Employee): string {
  return employee.firstName + ' ' + employee.lastName
}

export type { empGender, empRole, empQualification, Employee }

import { defaultUser, users as usersApi } from '@/api/userApi'
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'

export const useUserStore = defineStore('userm', () => {
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Username', key: 'username', sortable: false },
    { title: 'Password', key: 'password', sortable: false },
    { title: 'Roles', key: 'roles', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false }
  ]

  const users = ref([...usersApi])

  const editedIndex = ref(-1)
  const editedItem = ref(defaultUser)

  const dialog = ref(false)
  const dialogDelete = ref(false)

  const formTitle = computed(() => (editedIndex.value === -1 ? 'New Item' : 'Edit Item'))

  function closeDelete() {
    dialogDelete.value = false
  }

  function deleteItemConfirm() {
    users.value.splice(editedIndex.value)
    closeDelete()
  }

  function close() {
    dialog.value = false
    editedItem.value = Object.assign({}, defaultUser)
  }

  function save() {
    if (editedIndex.value > -1) {
      Object.assign(users.value[editedIndex.value], editedItem.value)
    } else {
      const lastId = users.value.reduce((maxId, user) => (user.id > maxId ? user.id : maxId), 0)
      editedItem.value.id = lastId + 1
      users.value.push(editedItem.value)
    }
    close()
  }

  function editItem(item: any) {
    editedIndex.value = item ? users.value.indexOf(item) : -1
    editedItem.value = item ? { ...item } : { ...defaultUser }
    dialog.value = true
  }

  function deleteItem(item: any) {
    editedIndex.value = users.value.indexOf(item)
    editedItem.value = Object.assign({}, item)
    dialogDelete.value = true
  }

  return {
    headers,
    users,
    editedIndex,
    editedItem,
    dialog,
    dialogDelete,
    formTitle,
    closeDelete,
    deleteItemConfirm,
    close,
    save,
    editItem,
    deleteItem
  }
})

import type { Employee } from '@/views/Employee/types/Employee'

export type Role = 'admin' | 'employee'

export type User = {
  id: number
  username: string
  password: string
  roles: Role[]
  employee: Employee | null
}

import { nextTick, ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type { CheckTime } from '../types/checktime'
import { attendanceRecord, defaultChItem } from '@/api/attendanceApi'
import { useAuthStore } from '@/services/auth'
import { useSalaryStore } from '@/views/Salary/stores/salary'

export const useChecktimesStore = defineStore('checktime', () => {
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Date', key: 'date' },
    { title: 'User', key: 'user', sortable: false },
    { title: 'Employee', key: 'employee', sortable: false },
    { title: 'Clock In', key: 'clockIn' },
    { title: 'Clock Out', key: 'clockOut' },
    { title: 'Time Worked', key: 'timeWorked', sortable: false },
    { title: 'Types', key: 'types', sortable: false }
  ]
  const checktimeDialog = ref(false)
  const chRecords = ref<CheckTime[]>(attendanceRecord)
  const currentChItem = ref<CheckTime | null>()
  const chItem = ref<CheckTime>({
    id: -1,
    date: '',
    user: null,
    clockIn: '',
    clockOut: '',
    timeWorked: 0,
    typeWork: null
  })

  let editedIndex = -1
  const dialogDelete = ref(false)
  const dialog = ref(false) //add member
  const dialogs = ref(false) //edit member
  function showDialog() {
    dialog.value = true // add
  }
  function showDialogs() {
    dialogs.value = true // edit
  }

  const editedChecktime = ref<CheckTime>(JSON.parse(JSON.stringify(chItem)))
  const checktimeItems = ref<CheckTime[]>([])

  function editItem(item: any) {
    editedIndex = chRecords.value.indexOf(item)
    editedChecktime.value = Object.assign({}, item)
    chItem.value = item

    dialogs.value = true
  }

  //   function closeDelete () {
  //     dialogDelete.value = false
  //     nextTick(() => {
  //         editedMember.value = Object.assign({}, initialMember)
  //     })
  // }
  function closeDelete() {
    dialogDelete.value = false
  }

  // function deleteItemConfirm(productItem: Product) {
  //   const index = memberItems.value.findIndex((item) => item === memberItem)
  //   memberItems.value.splice(index)
  //   members.value.splice(editedIndex)
  //   closeDelete()
  //   console.log(editedIndex)

  // }
  function deleteItemConfirm() {
    chRecords.value.splice(editedIndex)
    closeDelete()
  }

  function deleteItem(item: any) {
    editedIndex = chRecords.value.indexOf(item)
    editedChecktime.value = Object.assign({}, item)
    dialogDelete.value = true
    // editedIndex = -1
  }

  function clear() {
    checktimeItems.value = []
    chItem.value = { ...defaultChItem }
    editedChecktime.value = chItem.value
  }

  function closeDialog() {
    dialog.value = false
    dialogs.value = false
    // nextTick(() => {
    //   editedChecktime.value = Object.assign({}, chItem)
    //   editedIndex = -1
    // })
  }

  function save() {
    // const { valid } = await refForm.value!.validate()
    // if(!valid) return
    // if (editedIndex > -1) {
    //   Object.assign(chRecords.value[editedIndex], editedChecktime.value)
    // } else {
    //   chRecords.value.push(editedChecktime.value)
    // }
    // closeDialog()
    chRecords.value.push({ ...chItem.value })
    // Add timeWorked to pendingSlips if not exist | update timeWorked if exist
    useSalaryStore().autoAddPendingSlip(chItem.value)
    clear()
  }
  function showChecktimeDialog() {
    checktimeDialog.value = true
    // currentMember.value
    // receipt.value.receiptItem = receiptItems.value
  }

  const timeClockIn = ref(0)
  const timeClockOut = ref(0)

  const actions = {
    saveClockIn() {
      // store clock in number
      timeClockIn.value = new Date().getTime()
      // store clock in string
      chItem.value.clockIn = new Date().toLocaleTimeString('th-TH', {hour12: false})
      // get now current user
      chItem.value.user = useAuthStore().methods.getCurrentUser()
    },
    saveClockOut() {
      if (chItem.value.clockIn !== '') {
        // store clock out number
        timeClockOut.value = new Date().getTime()
        // add clock out time string
        chItem.value.clockOut = new Date().toLocaleTimeString('th-TH', {hour12: false})
        // add date
        chItem.value.date = new Date().toLocaleDateString()
        // calculate work time
        const timeWorkedMilliseconds = Math.abs(timeClockIn.value - timeClockOut.value)
        chItem.value.timeWorked = Math.floor(timeWorkedMilliseconds / 3600000)
        // calculate work type
        chItem.value.typeWork = chItem.value.timeWorked >= 8 ? 'Normal' : 'Missing'
        // add lasted id
        chItem.value.id =
          chRecords.value.length > 0 ? chRecords.value[chRecords.value.length - 1].id + 1 : -1
        save()
      }
    }
  }

  return {
    headers,
    chRecords,
    currentChecktime: currentChItem,
    editedIndex,
    dialog,
    dialogs,
    chItem,
    dialogDelete,
    editedChecktime,
    editItem,
    deleteItem,
    clear,
    deleteItemConfirm,
    closeDelete,
    showDialog,
    showDialogs,
    save,
    showChecktimeDialog,
    actions,
    timeClockIn,
    timeClockOut
  }
})

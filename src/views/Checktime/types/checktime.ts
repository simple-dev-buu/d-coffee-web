import type { User } from '@/views/User/types/user'

type TypeWork = 'Missing' | 'Normal' | 'Normal/OT'
type CheckTime = {
  id: number
  date: string
  user: User | null
  clockIn: string
  clockOut: string
  timeWorked: number
  typeWork: TypeWork | null
}

export type { TypeWork, CheckTime }

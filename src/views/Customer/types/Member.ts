type Member = {
    id: number,
    fName: string,
    lName: string,
    tel: string,
    point: number,
    birthDate: string
}

export type { Member }

import { computed, nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '../types/Member'
import MemberDialogVue from '../views/CustomerDialog.vue'
import { customers } from '@/api/customerApi'

export const useMembersStore = defineStore('members', () => {
  const members = ref<Member[]>(customers)
  const selectedTab = ref(0)
  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'First Name', key: 'fname', sortable: false },
    { title: 'Last Name', key: 'lname', sortable: false },
    { title: 'Tel.', key: 'tel', sortable: false },
    { title: 'Point', key: 'point', sortable: false },
    { title: 'BirthDate', key: 'birthDate', sortable: false },
    { title: 'Actions', key: 'actions', sortable: false }
  ]
  let lastId = 21
  const currentMember = ref<Member | null>()
  const searchMember = (tel: string) => {
    const index1 = members.value.findIndex((item) => item.tel === tel)
    if (index1 < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index1]
  }

  const member = ref<Member>({
    id: -1,
    fName: '',
    lName: '',
    tel: '',
    point: 0,
    birthDate: ''
  })
  const initialMember: Member = {
    id: -1,
    fName: '',
    lName: '',
    tel: '',
    point: 0,
    birthDate: ''
  }
  let editedIndex = -1
  const dialogDelete = ref(false)
  const dialog = ref(false) //add member
  const dialogs = ref(false) //edit member
  function showDialog() {
    dialog.value = true // add
  }
  function showDialogs() {
    dialogs.value = true // edit
  }

  const editedMember = ref<Member>(JSON.parse(JSON.stringify(initialMember)))
  const memberItems = ref<Member[]>([])

  function editItem(item: any) {
    editedIndex = members.value.indexOf(item)
    editedMember.value = Object.assign({}, item)
    member.value = item

    dialogs.value = true
    console.log(item)

    console.log(member.value)
    console.log(memberItems.value)
  }

  function closeDelete() {
    dialogDelete.value = false
  }

  function deleteItemConfirm() {
    members.value.splice(editedIndex, 1)
    closeDelete()
  }

  function deleteItem(item: any) {
    editedIndex = members.value.indexOf(item)
    editedMember.value = Object.assign({}, item)
    dialogDelete.value = true
    // editedIndex = -1
  }
  5

  function clear() {
    memberItems.value = []
    member.value = {
      id: 0,
      fName: '',
      lName: '',
      tel: '',
      point: 0,
      birthDate: ''
    }
    editedMember.value = member.value
    // clear()
  }

  function closeDialog() {
    dialog.value = false
    dialogs.value = false
    nextTick(() => {
      editedMember.value = Object.assign({}, initialMember)
      editedIndex = -1
    })
  }

  async function save() {
    // const { valid } = await refForm.value!.validate()
    // if(!valid) return
    if (editedIndex > -1) {
      Object.assign(members.value[editedIndex], editedMember.value)
      console.log(editedMember)
    } else {
      editedMember.value.id = lastId++
      console.log(editedMember.value)
      members.value.push(editedMember.value)
      console.log(editedMember.value)
      console.log(members.value)
    }
    // editedIndex = -1
    console.log(memberItems.value)
    closeDialog()
    clear()
  }

  return {
    members,
    currentMember,
    editedIndex,
    dialog,
    dialogs,
    member,
    dialogDelete,
    editedMember,
    selectedTab,
    headers,
    searchMember,
    editItem,
    deleteItem,
    clear,
    deleteItemConfirm,
    closeDelete,
    showDialog,
    showDialogs,
    save
  }
})

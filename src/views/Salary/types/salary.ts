import type { empRole } from "@/views/Employee/types/Employee"


type state = 'รอชำระ' | 'ชำระแล้ว'
type Salary = {
    id: number
    date: string
    period: string
    employeeId?: number
    role?: empRole
    name: string
    timeWorked: number
    defaultSalary: number
    lossSalary: number
    netSalary: number
    state: state
}

export type { state, Salary }
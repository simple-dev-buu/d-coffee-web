import { ref, computed, nextTick } from 'vue'
import { defineStore } from 'pinia'
import type { Salary } from '../types/salary'
import { pendingSlips as pendingSlipsApi, slipRecords as slipRecordsApi } from '@/api/salaryApi'
import { useEmployeeStore } from '../../Employee/stores/employee'
import type { VForm } from 'vuetify/components'

export const useSalaryStore = defineStore('salary', () => {

  const employeeStore = useEmployeeStore()

  const headers = [
    { title: 'ID', key: 'id' },
    { title: 'Period', key: 'period' },
    { title: 'Paid Date', key: 'date', sortable: false },
    { title: 'Employee ID', key: 'employeeId', sortable: false },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Net Salary', key: 'netSalary', sortable: false },
    { title: 'State', key: 'state', sortable: false },
    { title: 'Action', key: 'action', sortable: false }
  ]

  const headers2 = [
    { title: 'ID', key: 'id' },
    { title: 'Period', key: 'period' },
    { title: 'Paid Date', key: 'date', sortable: false },
    { title: 'Employee ID', key: 'employeeId', sortable: false },
    { title: 'Name', key: 'name', sortable: false },
    { title: 'Time Worked', key: 'timeWorked', sortable: false },
    { title: 'Net Salary', key: 'netSalary', sortable: false },
    { title: 'State', key: 'state', sortable: false },
    { title: 'Action', key: 'action', sortable: false }
  ]

  const slipRecords = ref<Salary[]>(slipRecordsApi)
  const pendingSlips = ref<Salary[]>(pendingSlipsApi)

  const dialog = ref(false)
  const dialogViewSlip = ref(false)
  const dialogPendingDelete = ref(false)
  const dialogPaidDelete = ref(false)

  const refForm = ref<VForm | null>(null)

  const initialSlip: Salary = {
    id: -1,
    date: '',
    period: '',
    employeeId: -1,
    role: undefined,
    name: '',
    timeWorked: 0,
    lossSalary: 0,
    defaultSalary: 0,
    netSalary: 0,
    state: 'รอชำระ'
  }

  function autoAddPendingSlip(item: any) {
    const name = item.user.employee.firstName + ' ' + item.user.employee.lastName
    const empId = findEmpIdbyName(name)
    const index = findIndexbyEmpId(empId)
    console.log(item)
    
    if (pendingSlips.value.every((slip) => slip.employeeId !== empId)) {
        const newPendingSlip: Salary = {
            id: pendingSlips.value.length + 1,
            date: '',
            period: getMonthRange(item.date),
            employeeId: empId,
            role: employeeStore.employees.find((employee) => employee.id === empId)?.roles,
            name: name,
            timeWorked: item.timeWorked,
            defaultSalary: employeeStore.employees.find((employee) => employee.id === empId)?.moneyrate || 0,
            lossSalary: 0,
            netSalary: 0,
            state: 'รอชำระ'
        }
        pendingSlips.value.push(newPendingSlip)
        console.log(pendingSlips.value)

    } else {
        pendingSlips.value[index].timeWorked = pendingSlips.value[index].timeWorked + 50
        console.log(pendingSlips.value)
    }
    calculateNetSalary(index, empId)

  }
  
  const editedSlip = ref<Salary>(JSON.parse(JSON.stringify(initialSlip)))
  const editedIndex = ref(-1)

  const selectedId = ref(-1)
  

  function editItem(item: any) {
    editedIndex.value = pendingSlips.value.indexOf(item)
    editedSlip.value = Object.assign({}, item)
    selectedId.value = item.employeeId
    dialog.value = true
  }

  function viewSlipDetails(item: any) {
    editedIndex.value = slipRecords.value.indexOf(item)
    editedSlip.value = Object.assign({}, item)
    selectedId.value = item.employeeId
    console.log(editedSlip.value)
    dialogViewSlip.value = true
  }

  function deletePendingItem(item: any) {
    editedIndex.value = pendingSlips.value.indexOf(item)
    editedSlip.value = Object.assign({}, item)
    dialogPendingDelete.value = true
  }

  function deletePaidItem(item: any) {
    editedIndex.value = slipRecords.value.indexOf(item)
    editedSlip.value = Object.assign({}, item)
    dialogPaidDelete.value = true
  }

  function delPendingItemConfirm() {
    pendingSlips.value.splice(editedIndex.value)
    closeDelete()
  }

  function delPaidItemConfirm() {
    slipRecords.value.splice(editedIndex.value)
    closeDelete()
  }
  
  function closeDelete() {
    dialogPendingDelete.value = false
    dialogPaidDelete.value = false
  }

  function savePaidSlip() {
    console.log(editedSlip.value)
    if (selectedId.value > -1) {
      editedSlip.value.id = slipRecords.value.length + 1
      editedSlip.value.state = 'ชำระแล้ว'
      editedSlip.value.date = formatDate(new Date())
      slipRecords.value.push(editedSlip.value)
      pendingSlips.value.splice(editedIndex.value)
    } 
    console.log(slipRecords.value)
    clear()
  }

  function closeDialog() {
    dialog.value = false
    dialogViewSlip.value = false
    clear()
  }
  function clear() {
    selectedId.value = -1
    editedIndex.value = -1
    editedSlip.value = Object.assign({}, initialSlip)
  }

  function getMonthRange(date: string) {
    const dateObj = new Date(date);
    const year = dateObj.getFullYear();
    const month = dateObj.getMonth();
    const firstDay = new Date(year, month, 1);
    const lastDay = new Date(year, month + 1, 0);
  
    const formattedFirstDay = formatDate(firstDay);
    const formattedLastDay = formatDate(lastDay);
  
    return `${formattedFirstDay} - ${formattedLastDay}`;
  }

  function formatDate(date: Date) {
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = String(date.getFullYear());
  
    return `${day}/${month}/${year}`;
  }

  function findEmpIdbyName(name: string) {
    return employeeStore.employees.find((employee) => employee.firstName+' '+employee.lastName === name)?.id
  }

  function findIndexbyEmpId(id?: number) {
    return pendingSlips.value.findIndex((pendingSlips) => pendingSlips.employeeId === id)
  }

  function NewRecord(){
    dialog.value = true
    nextTick(() => {
        if (refForm.value) refForm.value.reset()
    })
  } 

  function calculateNetSalary(index: number, empId: number | undefined) {
    console.log("timeWorked: "+pendingSlips.value[index].timeWorked)
    console.log("minWorkHour: "+empMinWorkHour(empId))
    if (pendingSlips.value[index].timeWorked > 0) {
      if (pendingSlips.value[index].timeWorked >= (0.90 * empMinWorkHour(empId))) {
        pendingSlips.value[index].netSalary = pendingSlips.value[index].defaultSalary!
      } else if (pendingSlips.value[index].timeWorked >= (0.75 * empMinWorkHour(empId))) {
        pendingSlips.value[index].netSalary = pendingSlips.value[index].defaultSalary! * 0.75
      } else if (pendingSlips.value[index].timeWorked >= (0.50 * empMinWorkHour(empId))) {
        pendingSlips.value[index].netSalary = pendingSlips.value[index].defaultSalary! * 0.5
      }
    }
    pendingSlips.value[index].lossSalary = pendingSlips.value[index].defaultSalary - pendingSlips.value[index].netSalary
    console.log("lossSalary: "+pendingSlips.value[index].lossSalary)
  }

  function empMinWorkHour(empId: number | undefined) {
    return employeeStore.employees.find((employee) => employee.id === empId)?.minWorkingHour || 0
  }

  return {
    headers,
    headers2,
    slipRecords,
    pendingSlips,
    initialSlip,
    savePaidSlip,
    clear,
    dialog,
    dialogViewSlip,
    editedSlip,
    editItem,
    deletePendingItem,
    deletePaidItem,
    dialogPendingDelete,
    dialogPaidDelete,
    closeDelete,
    autoAddPendingSlip,
    getMonthRange,
    NewRecord,
    closeDialog,
    selectedId,
    viewSlipDetails,
    delPendingItemConfirm,
    delPaidItemConfirm
  }
})

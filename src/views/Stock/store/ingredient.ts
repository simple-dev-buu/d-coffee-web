import { computed, nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Ingredient } from '../types/Ingredient'
import { ingredients as api } from '@/api/stockApi'

export const useIngredientStore = defineStore('ingredient', () => {
  const ingredientDialog = ref(false)
  const ingredients = ref<Ingredient[]>(api)
  const defaultIngredient = ref<Ingredient>({
    id: -1,
    ingredientID: '',
    ingredientName: '',
    minimum: 0,
    balance: 0,
    unit: '',
    status: []
  })
  const initialIngredient: Ingredient = {
    id: -1,
    ingredientID: '',
    ingredientName: '',
    minimum: 0,
    balance: 0,
    unit: '',
    status: []
  }

  const dialogDelete = ref(false)
  let editedIndex = -1
  const lastId = ref(0)
  const editedIngredient = ref<Ingredient>(JSON.parse(JSON.stringify(defaultIngredient)))
  const ingredientItems = ref<Ingredient[]>([])

  const sortIngredient = computed(() =>
  ingredients.value.slice().sort((a,b) => b.id - a.id)
)

function sortId(){
  sortIngredient
}

const searchIngredient = (id: string) => {
  const index1 = ingredients.value.findIndex((item) => item.ingredientID === id)
  if (index1 < 0) {
    currentIngredient.value = null
  }
  currentIngredient.value = ingredients.value[index1]
}

  const currentIngredient = ref<Ingredient | null>()

  nextTick(() => {
    editedIngredient.value = Object.assign({}, initialIngredient)
  })

  function editItem(item: any) {
    editedIndex = ingredients.value.indexOf(item)
    editedIngredient.value = Object.assign({}, item)
    defaultIngredient.value = item

    dialogs.value = true
    console.log(item)

    console.log(defaultIngredient.value)
    console.log(ingredientItems.value)
  }

  function closeDelete() {
    dialogDelete.value = false
  }
  
  function deleteItemConfirm() {
    ingredients.value.splice(editedIndex, 1)
    closeDelete()
  }

  function deleteItem(item: Ingredient) {
    editedIndex = ingredients.value.indexOf(item)
    editedIngredient.value = Object.assign({}, item)
    dialogDelete.value = true
  }

  function clear() {
    ingredientItems.value = []
    defaultIngredient.value = {
      id: 0,
      ingredientID: '',
      ingredientName: '',
      minimum: 0,
      balance: 0,
      unit: '',
      status: []
    }
    editedIngredient.value = defaultIngredient.value
    currentIngredient.value = null
    console.log(currentIngredient.value)

  }

  const dialog = ref(false) //editanddelete
  function showDialog() {
    dialog.value = true
  }
  const dialogs = ref(false) //addIngredient
  function showDialogs() {
    dialogs.value = true
  }

  const dialogCheckStock = ref(false) //numkaoIngredient
  function showDialoges() {
    dialogCheckStock.value = true
  }

  const dialogLow = ref(false)
  function showDialogLow() {
    dialogLow.value = true
  }
  
  function closeDialog() {
    if (currentIngredient.value != null) {
      dialogs.value = false
    }
  }

  async function save() {
    // const { valid } = await refForm.value!.validate()
    // if(!valid) return
    if (editedIndex > -1) {
      Object.assign(ingredients.value[editedIndex], editedIngredient.value)
      console.log(ingredients)
    } else {
      let lastId = 16
      editedIngredient.value.id = lastId++
      console.log(editedIngredient.value)
      ingredients.value.push(editedIngredient.value)
      console.log(ingredientItems.value)
      console.log(ingredients.value)
    }
    // editedIndex = -1
    console.log(ingredients.value)
    closeDialog()
    clear()
  }

  function close() {
    dialog.value = false
    editedIngredient.value = Object.assign({}, initialIngredient)
  }

  return {
    ingredients,
    currentIngredient,
    ingredientDialog,
    dialog,
    defaultIngredient,
    dialogDelete,
    dialogs,
    dialogCheckStock,
    dialogLow,
    editedIngredient,
    sortIngredient,
    searchIngredient,
    showDialog,
    editItem,
    closeDelete,
    deleteItemConfirm,
    deleteItem,
    clear,
    showDialogs,
    showDialoges,
    showDialogLow,
    save,
    close,
    sortId
  }
})

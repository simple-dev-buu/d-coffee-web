type Status = 'ปกติ' | 'เหลือน้อย'
type Ingredient = {
    id: number,
    ingredientID: string,
    ingredientName: string,
    minimum: number,
    balance: number,
    unit: string,
    status: Status[]
}

export type { Ingredient }
export type { Status }
import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/views/Receipt/types/ReceiptItem'
import type { Product } from '@/views/POS/types/Products'
import type { Receipt } from '@/views/Receipt/types/Receipt'
import type { Promotion } from '../../Promotion/types/Promotion'
import { useMemberStore } from '@/views/POS/store/member'
import { usePromotionStore } from '@/views/Promotion/store/promotion'
import { receipts } from '@/api/receiptApi'

export const useReceiptStore = defineStore('Receipt', () => {
  const receiptsView = ref<Receipt[]>(receipts)
  const dialogReceiptView = ref(false)
  const dialogPromptPay = ref(false)
  const dialogCredit = ref(false)
  const selectedReceipt = ref<Receipt>()
  const addSelectReceipt = (item: Receipt) => {
    dialogReceiptView.value = true
    selectedReceipt.value = item
  }
  const errorInput = ref(false)
  const errorInputPoint = ref(false)

  let lastIdReceipt = 13
  let lastIdReceiptItem = 24
  const memberStore = useMemberStore()
  const promotionStore = usePromotionStore()
  const receiptDialog = ref(false)
  const paymentType = ref('Cash')
  const receipt = ref<Receipt>({
    id: lastIdReceipt++,
    createDate: new Date(),
    totalUnit: 0,
    totalBefore: 0,
    discount: 0,
    netPrice: 0,
    receivedAmount: 0,
    change: 0,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 0,
    memberId: 0,
    givePoint: 0,
    usePoint: 0,
    totalPoint: 0
  })
  const receiptItems = ref<ReceiptItem[]>([])
  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        id: lastIdReceiptItem++,
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        product: product
      }
      receiptItems.value.push(newReceipt)
      calReceipt()
    }
  }
  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }
  let usePromotion: Promotion
  let usePoint = 0
  function calReceipt() {
    let totalBefore = 0
    let totalUnits = 0
    let discountPromotion = 0
    // let total = 0

    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.price * item.unit
      totalUnits = totalUnits + item.unit

      calPoint()
    }
    receipt.value.promotion
    receipt.value.totalBefore = totalBefore
    receipt.value.totalUnit = totalUnits

    // if(memberStore.currentMember){
    //     total = totalBefore * 0.95

    //     receipt.value.netPrice = total

    //     // calPoint()
    //   }else{
    //     receipt.value.netPrice = totalBefore
    //   }
    if (promotionStore.currentPromotion) {
      discountPromotion = promotionStore.currentPromotion?.discount
      receipt.value.discount = discountPromotion
      receipt.value.netPrice = totalBefore - receipt.value.discount

      usePromotion = promotionStore.currentPromotion
      receipt.value.promotion = usePromotion
      console.log("เลือกใช้ "+receipt.value.promotion.name)
    }
    receipt.value.netPrice = totalBefore - receipt.value.discount //always calculate discount?
    receipt.value.totalPoint = receipt.value.totalPoint - usePoint // total Point!!!!!!!!!!!!!!!!!!! (no - receipt.value.discount)
    console.log("total point หลังใช้ promotion "+receipt.value.totalPoint)
    console.log("discount"+ receipt.value.discount)

  }

  function calPoint() {
    let givePoint = 0
    // let usePoint = 0
    let totalPoint = 0
    let memberPoint = 0
    for (const item of receiptItems.value) {
      givePoint = givePoint + item.unit
      totalPoint = givePoint
    }

    if (memberStore.currentMember != null) {
      memberPoint = memberStore.currentMember.point
      receipt.value.totalPoint = receipt.value.givePoint + memberStore.currentMember.point
      console.log(receipt.value.totalPoint)
    }

    receipt.value.givePoint = givePoint
    // receipt.value.usePoint = usePoint
    receipt.value.totalPoint = totalPoint + memberPoint
  }

  function usePoints(use: number) {
    if(use <= receipt.value.totalPoint){
      usePoint = receipt.value.totalUnit - use
      receipt.value.totalPoint = receipt.value.totalPoint - use
      console.log("Total Point"+receipt.value.totalPoint)
      console.log("Point ที่ใช้ "+use)
      console.log(promotionStore.currentPromotion?.name)
  
      //if use point
      receipt.value.discount = use
      console.log("Discount "+use)
      receipt.value.netPrice =  receipt.value.totalBefore - receipt.value.discount
      // calReceipt()
    }else errorInputPoint.value = true
    
  }

  function calMoney(money: number) {
    if (money > 0) {
      receipt.value.receivedAmount = money
      receipt.value.change = money - receipt.value.netPrice
    }
  }

  let queue = 1
  function showReceiptDialog() {
    receipt.value.paymentType = paymentType.value
    console.log(queue)
    queue = queue+1
    console.log(queue)
    if (
      receipt.value.change >= 0 &&
      receipt.value.receivedAmount > 0 &&
      receipt.value.paymentType === 'Cash' &&
      receiptItems.value.length > 0
    ) {
      errorInput.value = false
      receiptDialog.value = true
      receipt.value.receiptItem = receiptItems.value
      receipt.value.paymentType = paymentType.value
    }else if(receipt.value.paymentType === 'PromptPay') {
      dialogPromptPay.value = true
      receipt.value.receivedAmount = receipt.value.netPrice      
      receipt.value.receiptItem = receiptItems.value
      receipt.value.paymentType = paymentType.value
    }else if(receipt.value.paymentType === 'Credit Card') {
      dialogCredit.value = true
      receipt.value.receivedAmount = receipt.value.netPrice      
      receipt.value.receiptItem = receiptItems.value
      receipt.value.paymentType = paymentType.value
    }
    else {
      errorInputPoint.value = true
    }
    

    // if (
    //   receipt.value.change >= 0 &&
    //   receipt.value.receivedAmount > 0 &&
    //   receipt.value.paymentType !== '' &&
    //   receiptItems.value.length > 0
    // ) {
    //   errorInput.value = false
    //   dialogPromptPay.value = true
    //   receiptDialog.value = true
    //   receipt.value.receiptItem = receiptItems.value
    //   receipt.value.paymentType = paymentType.value
    // } else {
    //   errorInput.value = true
    // }
  }
  function clear() {
    receiptItems.value = []
    receipt.value = Object.assign({
      id: lastIdReceipt++,
      createDate: new Date(),
      totalUnit: 0,
      totalBefore: 0,
      discount: 0,
      netPrice: 0,
      receivedAmount: 0,
      change: 0,
      promotionId: 0,
      paymentType: 'Cash',
      userId: 0,
      memberId: 0,
      givePoint: 0,
      usePoint: 0,
      totalPoint: 0
    })
    memberStore.clear()
    promotionStore.clear()
  }

  function clearSelectedReceipt() {
    selectedReceipt.value = {
      id: 0,
      createDate: new Date(),
      totalUnit: 0,
      totalBefore: 0,
      discount: 0,
      netPrice: 0,
      receivedAmount: 0,
      change: 0,
      promotionId: 0,
      paymentType: 'Cash',
      userId: 0,
      memberId: 0,
      givePoint: 0,
      usePoint: 0,
      totalPoint: 0
    }
  }

  // function updateReceipt(){
  //   receipt.value.id
  //   receipt.value.createDate
  //   receipt.value.totalUnit = totalUnits
  //   receipt.value.totalBefore = totalBefore
  //   receipt.value.discount = discount
  //   receipt.value.netPrice = netPrice
  //   receipt.value.receivedAmount = receivedAmount
  //   receipt.value.change = change
  //   receipt.value.promotionId
  //   receipt.value.paymentType
  //   receipt.value.userId
  //   receipt.value.memberId
  //   receipt.value.givePoint = givePoint
  //   receipt.value.usePoint = usePoint
  //   receipt.value.totalPoint = totalPoint
  // }
  function addReceipt() {
    receiptsView.value.push(receipt.value)
    queue++
    console.log(receiptsView)
  }

  return {
    errorInput,
    errorInputPoint,
    receiptsView,
    receiptItems,
    receipt,
    receiptDialog,
    paymentType,
    selectedReceipt,
    dialogReceiptView,
    queue,
    dialogPromptPay,
    dialogCredit,
    addSelectReceipt,
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    calReceipt,
    showReceiptDialog,
    clear,
    calPoint,
    calMoney,
    usePoints,
    clearSelectedReceipt,
    addReceipt,
    
  }
})

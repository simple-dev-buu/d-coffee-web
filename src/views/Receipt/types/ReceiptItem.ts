import type { Product } from "@/views/POS/types/Products";

export type ReceiptItem = {
    id: number;
    name: string;
    price: number;
    unit: number;
    productId: number;
    product?: Product;
}


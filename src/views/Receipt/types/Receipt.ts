import type { Member } from '@/views/Customer/types/Member'
import type { Promotion } from '@/views/Promotion/types/Promotion'
import type { User } from '@/views/POS/types/User'
import type { ReceiptItem } from './ReceiptItem'

export type Receipt = {
  id: number
  createDate: Date
  totalUnit: number
  totalBefore: number
  discount: number
  netPrice: number
  receivedAmount: number
  change: number
  promotionId: number
  promotion?: Promotion
  paymentType: string
  userId: number
  user?: User
  memberId: number
  member?: Member
  givePoint: number
  usePoint: number
  totalPoint: number
  receiptItem?: ReceiptItem[]
}

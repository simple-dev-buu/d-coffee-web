import { defaultUser, users } from '@/api/userApi'
import router from '@/router'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import LoginView from '@/views/Auth/views/LoginView.vue'
import type { Role, User } from '@/views/User/types/user'

export const useAuthStore = defineStore('auth', () => {
  const data = {
    currentUser: ref(defaultUser),
    users: ref(users),
    employeeRoutes: ['/pos', '/stock', '/checktime']
  }

  const methods = {
    isAuthenticated(): boolean {
      return data.currentUser.value!.id > -1
    },
    isAuthorized: (routeRoles: string[]): boolean => {
      return routeRoles.some((role) => data.currentUser.value.roles.includes(role as Role))
    },
    setCurrentUser(newUser: User): void {
      data.currentUser.value = newUser
      const parsed = JSON.stringify(newUser)
      localStorage.setItem('user', parsed)
    },
    checkUser(username: string, password: string): boolean {
      for (const user of data.users.value) {
        if (user.username === username && user.password === password) {
          this.setCurrentUser(JSON.parse(JSON.stringify(user)))

          return true
        }
      }
      return false
    },
    getCurrentUser(): User | null {
      return JSON.parse(JSON.stringify(data.currentUser.value))
    },
    clearUser() {
      localStorage.removeItem('user')
    },
    async logout() {
      data.currentUser.value!.id = -1
      await this.clearUser()
      router.addRoute({
        path: '/',
        name: 'login',
        component: LoginView
      })
      router.push('/')
    },
    checkRememberMe(): boolean {
      const json = localStorage.getItem('user')
      if (json !== null) {
        this.setCurrentUser(JSON.parse(json))
        return true
      }
      this.clearUser
      return false
    }
  }
  return { data, methods }
})

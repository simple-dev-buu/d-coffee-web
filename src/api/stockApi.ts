import type { Ingredient } from '@/views/Stock/types/Ingredient'

export const ingredients = <Ingredient[]>[
  {
    id: 1,
    ingredientID: 'IGN001',
    ingredientName: 'ผงโกโก้',
    minimum: 10,
    balance: 21,
    unit: 'กิโลกรัม',
    status: ['ปกติ']
  },
  {
    id: 2,
    ingredientID: 'IGN002',
    ingredientName: 'ผงชาเขียว',
    minimum: 10,
    balance: 9,
    unit: 'กิโลกรัม',
    status: ['เหลือน้อย']
  },
  {
    id: 3,
    ingredientID: 'IGN003',
    ingredientName: 'ผงชาไทย',
    minimum: 10,
    balance: 15,
    unit: 'กิโลกรัม',
    status: ['ปกติ']
  },
  {
    id: 4,
    ingredientID: 'IGN004',
    ingredientName: 'ผงนมสด',
    minimum: 10,
    balance: 4,
    unit: 'กิโลกรัม',
    status: ['เหลือน้อย']
  },
  {
    id: 5,
    ingredientID: 'IGN005',
    ingredientName: 'น้ำตาล',
    minimum: 10,
    balance: 50,
    unit: 'กิโลกรัม',
    status: ['ปกติ']
  },
  {
    id: 6,
    ingredientID: 'IGN006',
    ingredientName: 'นมสด',
    minimum: 10,
    balance: 40,
    unit: 'ลิตร',
    status: ['ปกติ']
  },
  {
    id: 7,
    ingredientID: 'IGN007',
    ingredientName: 'นมข้นหวาน',
    minimum: 10,
    balance: 60,
    unit: 'ลิตร',
    status: ['ปกติ']
  },
  {
    id: 8,
    ingredientID: 'IGN008',
    ingredientName: 'ไข่มุก',
    minimum: 10,
    balance: 20,
    unit: 'กิโลกรัม',
    status: ['ปกติ']
  },
  {
    id: 9,
    ingredientID: 'IGN009',
    ingredientName: 'เมล็ดกาแฟ',
    minimum: 10,
    balance: 30,
    unit: 'กิโลกรัม',
    status: ['ปกติ']
  },
  {
    id: 10,
    ingredientID: 'IGN010',
    ingredientName: 'น้ำแดงบลูบอย',
    minimum: 10,
    balance: 25,
    unit: 'ลิตร',
    status: ['ปกติ']
  },
  {
    id: 11,
    ingredientID: 'IGN011',
    ingredientName: 'ไซลัปคาลาเมล',
    minimum: 10,
    balance: 15,
    unit: 'ลิตร',
    status: ['ปกติ']
  },
  {
    id: 12,
    ingredientID: 'IGN012',
    ingredientName: 'คาลาเมล',
    minimum: 10,
    balance: 15,
    unit: 'ลิตร',
    status: ['ปกติ']
  },
  {
    id: 13,
    ingredientID: 'IGN013',
    ingredientName: 'ใบชา',
    minimum: 10,
    balance: 25,
    unit: 'กิโลกรัม',
    status: ['ปกติ']
  },
  {
    id: 14,
    ingredientID: 'IGN014',
    ingredientName: 'วิปครีม',
    minimum: 10,
    balance: 30,
    unit: 'กิโลกรัม',
    status: ['ปกติ']
  },
  {
    id: 15,
    ingredientID: 'IGN015',
    ingredientName: 'โซดา',
    minimum: 10,
    balance: 40,
    unit: 'ลิตร',
    status: ['ปกติ']
  }
]

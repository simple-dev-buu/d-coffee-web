import type { User } from '@/views/User/types/user'
import { employees } from './employeeApi'

const users: User[] = [
  {
    id: 0,
    username: 'admin',
    password: 'admin',
    roles: ['admin'],
    employee: employees[0]
  },
  {
    id: 1,
    username: 'rick',
    password: 'roll',
    roles: ['employee'],
    employee: employees[1]
  },
  {
    id: 2,
    username: 'john',
    password: 'cena',
    roles: ['employee'],
    employee: employees[2]
  },
  {
    id: 3,
    username: 'adam',
    password: 'smasher',
    roles: ['employee'],
    employee: employees[3]
  },
  {
    id: 4,
    username: 'max',
    password: 'wang',
    roles: ['employee'],
    employee: employees[4]
  }
]

const defaultUser: User = {
  id: -1,
  username: '',
  password: '',
  roles: [],
  employee: null
}

export { users, defaultUser }

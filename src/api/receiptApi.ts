import type { Receipt } from '@/views/Receipt/types/Receipt'

export const receipts = <Receipt[]>[
  {
    id: 1,
    createDate: new Date('2024-01-13T12:50:49.182Z'),
    totalUnit: 3,
    totalBefore: 155,
    discount: 0,
    netPrice: 155,
    receivedAmount: 200,
    change: 45,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: -1,
    givePoint: 3,
    usePoint: 0,
    totalPoint: 3,
    receiptItem: [
      {
        id: 1,
        name: 'ลาเต้',
        price: 50,
        unit: 1,
        productId: 2,
        product: { id: 2, name: 'ลาเต้', price: 50, type: 1 }
      },
      {
        id: 2,
        name: 'เค้กชิฟฟ่อนช็อกโกแลต',
        price: 60,
        unit: 1,
        productId: 21,
        product: { id: 21, name: 'เค้กชิฟฟ่อนช็อกโกแลต', price: 60, type: 2 }
      },
      {
        id: 3,
        name: 'บัตเตอร์เค้ก',
        price: 45,
        unit: 1,
        productId: 23,
        product: { id: 23, name: 'บัตเตอร์เค้ก', price: 45, type: 2 }
      }
    ]
  },
  {
    id: 2,
    createDate: new Date('2024-01-13T12:52:09.542Z'),
    totalUnit: 1,
    totalBefore: 50,
    discount: 10,
    netPrice: 40,
    receivedAmount: 40,
    change: 0,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: 1,
    givePoint: 1,
    usePoint: 10,
    totalPoint: 126,
    receiptItem: [
      {
        id: 4,
        name: 'ลาเต้',
        price: 50,
        unit: 1,
        productId: 2,
        product: { id: 2, name: 'ลาเต้', price: 50, type: 1 }
      }
    ]
  },
  {
    id: 3,
    createDate: new Date('2024-01-13T12:53:17.441Z'),
    totalUnit: 2,
    totalBefore: 100,
    discount: 0,
    netPrice: 100,
    receivedAmount: 100,
    change: 0,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: -1,
    givePoint: 2,
    usePoint: 0,
    totalPoint: 2,
    receiptItem: [
      {
        id: 5,
        name: 'ม็อคค่า',
        price: 50,
        unit: 1,
        productId: 5,
        product: { id: 5, name: 'ม็อคค่า', price: 50, type: 1 }
      },
      {
        id: 6,
        name: 'มัคคิอาโต้',
        price: 50,
        unit: 1,
        productId: 6,
        product: { id: 6, name: 'มัคคิอาโต้', price: 50, type: 1 }
      }
    ]
  },
  {
    id: 4,
    createDate: new Date('2024-01-13T12:54:07.293Z'),
    totalUnit: 1,
    totalBefore: 60,
    discount: 20,
    netPrice: 40,
    receivedAmount: 100,
    change: 60,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: 1,
    givePoint: 1,
    usePoint: 20,
    totalPoint: 116,
    receiptItem: [
      {
        id: 7,
        name: 'ชาเขียว',
        price: 60,
        unit: 1,
        productId: 11,
        product: { id: 11, name: 'ชาเขียว', price: 60, type: 1 }
      }
    ]
  },
  {
    id: 5,
    createDate: new Date('2024-01-13T12:54:47.158Z'),
    totalUnit: 2,
    totalBefore: 90,
    discount: 30,
    netPrice: 60,
    receivedAmount: 100,
    change: 40,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: 20,
    givePoint: 2,
    usePoint: 30,
    totalPoint: 10,
    receiptItem: [
      {
        id: 8,
        name: 'ข้าวผัด',
        price: 40,
        unit: 1,
        productId: 31,
        product: { id: 31, name: 'ข้าวผัด', price: 40, type: 3 }
      },
      {
        id: 9,
        name: 'ชาดำ',
        price: 50,
        unit: 1,
        productId: 15,
        product: { id: 15, name: 'ชาดำ', price: 50, type: 1 }
      }
    ]
  },
  {
    id: 6,
    createDate: new Date('2024-01-13T12:58:54.097Z'),
    totalUnit: 1,
    totalBefore: 50,
    discount: 0,
    netPrice: 50,
    receivedAmount: 100,
    change: 50,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: -1,
    givePoint: 1,
    usePoint: 0,
    totalPoint: 1,
    receiptItem: [
      {
        id: 10,
        name: 'ลาเต้',
        price: 50,
        unit: 1,
        productId: 2,
        product: { id: 2, name: 'ลาเต้', price: 50, type: 1 }
      }
    ]
  },
  {
    id: 7,
    createDate: new Date('2024-01-13T13:00:05.518Z'),
    totalUnit: 2,
    totalBefore: 100,
    discount: 0,
    netPrice: 100,
    receivedAmount: 100,
    change: 0,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: -1,
    givePoint: 2,
    usePoint: 0,
    totalPoint: 2,
    receiptItem: [
      {
        id: 11,
        name: 'เบอร์เกอร์',
        price: 50,
        unit: 1,
        productId: 35,
        product: { id: 35, name: 'เบอร์เกอร์', price: 50, type: 3 }
      },
      {
        id: 12,
        name: 'แฟลตไวท์',
        price: 50,
        unit: 1,
        productId: 7,
        product: { id: 7, name: 'แฟลตไวท์', price: 50, type: 1 }
      }
    ]
  },
  {
    id: 8,
    createDate: new Date('2024-01-13T13:01:01.208Z'),
    totalUnit: 2,
    totalBefore: 85,
    discount: 0,
    netPrice: 85,
    receivedAmount: 100,
    change: 15,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: -1,
    givePoint: 2,
    usePoint: 0,
    totalPoint: 2,
    receiptItem: [
      {
        id: 13,
        name: 'ชาดำ',
        price: 50,
        unit: 1,
        productId: 15,
        product: { id: 15, name: 'ชาดำ', price: 50, type: 1 }
      },
      {
        id: 14,
        name: 'แซนด์วิช',
        price: 35,
        unit: 1,
        productId: 22,
        product: { id: 22, name: 'แซนด์วิช', price: 35, type: 2 }
      }
    ]
  },
  {
    id: 9,
    createDate: new Date('2024-01-13T13:01:50.157Z'),
    totalUnit: 3,
    totalBefore: 120,
    discount: 20,
    netPrice: 100,
    receivedAmount: 100,
    change: 0,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: 6,
    givePoint: 3,
    usePoint: 20,
    totalPoint: 8,
    receiptItem: [
      {
        id: 15,
        name: 'ครัวซองต์',
        price: 35,
        unit: 2,
        productId: 25,
        product: { id: 25, name: 'ครัวซองต์', price: 35, type: 2 }
      },
      {
        id: 16,
        name: 'ลาเต้',
        price: 50,
        unit: 1,
        productId: 2,
        product: { id: 2, name: 'ลาเต้', price: 50, type: 1 }
      }
    ]
  },
  {
    id: 10,
    createDate: new Date('2024-01-13T13:02:13.917Z'),
    totalUnit: 2,
    totalBefore: 100,
    discount: 0,
    netPrice: 100,
    receivedAmount: 100,
    change: 0,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: 4,
    givePoint: 2,
    usePoint: 0,
    totalPoint: 49,
    receiptItem: [
      {
        id: 17,
        name: 'มัคคิอาโต้',
        price: 50,
        unit: 2,
        productId: 6,
        product: { id: 6, name: 'มัคคิอาโต้', price: 50, type: 1 }
      }
    ]
  },
  {
    id: 11,
    createDate: new Date('2024-01-13T13:02:56.619Z'),
    totalUnit: 1,
    totalBefore: 50,
    discount: 0,
    netPrice: 50,
    receivedAmount: 100,
    change: 50,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: 1,
    givePoint: 1,
    usePoint: 0,
    totalPoint: 136,
    receiptItem: [
      {
        id: 18,
        name: 'เบอร์เกอร์',
        price: 50,
        unit: 1,
        productId: 35,
        product: { id: 35, name: 'เบอร์เกอร์', price: 50, type: 3 }
      }
    ]
  },
  {
    id: 12,
    createDate: new Date('2024-01-13T13:03:52.674Z'),
    totalUnit: 4,
    totalBefore: 215,
    discount: 45,
    netPrice: 170,
    receivedAmount: 200,
    change: 30,
    promotionId: 0,
    paymentType: 'Cash',
    userId: 1,
    memberId: 10,
    givePoint: 4,
    usePoint: 0,
    totalPoint: 35,
    receiptItem: [
      {
        id: 19,
        name: 'สปาเก็ตตี้คาโบนาร่า',
        price: 55,
        unit: 1,
        productId: 33,
        product: { id: 33, name: 'สปาเก็ตตี้คาโบนาร่า', price: 55, type: 3 }
      },
      {
        id: 20,
        name: 'สปาเก็ตตี้ซอสมะเขือเทศ',
        price: 50,
        unit: 1,
        productId: 34,
        product: { id: 34, name: 'สปาเก็ตตี้ซอสมะเขือเทศ', price: 50, type: 3 }
      },
      {
        id: 21,
        name: 'ชาเขียว',
        price: 60,
        unit: 1,
        productId: 11,
        product: { id: 11, name: 'ชาเขียว', price: 60, type: 1 }
      },
      {
        id: 22,
        name: 'ชาดำ',
        price: 50,
        unit: 1,
        productId: 15,
        product: { id: 15, name: 'ชาดำ', price: 50, type: 1 }
      }
    ]
  }
]

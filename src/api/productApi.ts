import type { Product } from '@/views/POS/types/Products'

const defaultProduct = {
  id: -1,
  name: '',
  price: '',
  type: -1
}

const drinks = <Product[]>[
  { id: 1, name: 'เอสเปรสโซ', price: 50, type: 1 },
  { id: 2, name: 'ลาเต้', price: 50, type: 1 },
  { id: 3, name: 'คาปูชิโน่', price: 50, type: 1 },
  { id: 4, name: 'อเมริกาโน่', price: 50, type: 1 },
  { id: 5, name: 'ม็อคค่า', price: 50, type: 1 },
  { id: 6, name: 'มัคคิอาโต้', price: 50, type: 1 },
  { id: 7, name: 'แฟลตไวท์', price: 50, type: 1 },
  { id: 8, name: 'กาแฟดำ', price: 50, type: 1 },
  { id: 9, name: 'อาราบิก้า', price: 50, type: 1 },
  { id: 10, name: 'ชาขาว', price: 55, type: 1 },
  { id: 11, name: 'ชาเขียว', price: 60, type: 1 },
  { id: 12, name: 'ชาเหลือง', price: 50, type: 1 },
  { id: 13, name: 'ชาอู่หลง', price: 55, type: 1 },
  { id: 14, name: 'ชาแดง', price: 45, type: 1 },
  { id: 15, name: 'ชาดำ', price: 50, type: 1 },
  { id: 16, name: 'โอริโอ้ปั่น', price: 40, type: 1 },
  { id: 17, name: 'สตรอว์เบอร์รี่ปั่น', price: 40, type: 1 },
  { id: 18, name: 'ลิ้นจี่นมสดปั่น', price: 40, type: 1 },
  { id: 19, name: 'เบอร์รี่โยเกิร์ต', price: 45, type: 1 },
  { id: 20, name: 'สตรอว์เบอร์รี่โยเกิร์ต', price: 45, type: 1 }
]

const appetizer = <Product[]>[
  { id: 21, name: 'เค้กชิฟฟ่อนช็อกโกแลต', price: 60, type: 2 },
  { id: 22, name: 'แซนด์วิช', price: 35, type: 2 },
  { id: 23, name: 'บัตเตอร์เค้ก', price: 45, type: 2 },
  { id: 24, name: 'ขนมปัง', price: 25, type: 2 },
  { id: 25, name: 'ครัวซองต์', price: 35, type: 2 },
  { id: 26, name: 'คัพเค้ก', price: 30, type: 2 },
  { id: 27, name: 'คุกกี้', price: 20, type: 2 },
  { id: 28, name: 'เค้ก', price: 55, type: 2 },
  { id: 29, name: 'พายสับปะรด', price: 50, type: 2 }
]

const foods = <Product[]>[
  { id: 31, name: 'ข้าวผัด', price: 40, type: 3 },
  { id: 32, name: 'ข้าวไข่ข้นแฮม', price: 35, type: 3 },
  { id: 33, name: 'สปาเก็ตตี้คาโบนาร่า', price: 55, type: 3 },
  { id: 34, name: 'สปาเก็ตตี้ซอสมะเขือเทศ', price: 50, type: 3 },
  { id: 35, name: 'เบอร์เกอร์', price: 50, type: 3 },
  { id: 36, name: 'เฟรนซ์ฟราย', price: 35, type: 3 },
  { id: 37, name: 'ฮ็อตด็อก', price: 30, type: 3 },
  { id: 38, name: 'แพนเค้ก', price: 40, type: 3 },
  { id: 39, name: 'สลัด', price: 55, type: 3 }
]

export { drinks, appetizer, foods, defaultProduct as defaultProduct }

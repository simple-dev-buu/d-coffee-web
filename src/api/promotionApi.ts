import type { Promotion } from '@/views/Promotion/types/Promotion'

export const promotions = <Promotion[]>[
  {
    id: 1,
    name: 'Points',
    description: 'ลดราคาด้วยแต้มสมาชิก',
    discount: 0,
    startDate: '01/01/2024',
    endDate: '01/05/2024',
    status: ['active']
  },
  {
    id: 2,
    name: 'BirthDay',
    description: 'ลดราคาในวันเกิดของสมาชิก',
    discount: 0.5,
    startDate: '01/01/2023',
    endDate: '01/01/2024',
    status: ['active']
  },
  {
    id: 3,
    name: "Mother's Day",
    description: 'ลูกค้าสมาชิกสามารถพาคุณแม่มารับเครื่องดื่มฟรี 1 แก้ว',
    discount: 0.12,
    startDate: '12/08/2024',
    endDate: '13/08/2024',
    status: ['inactive']
  },
  {
    id: 4,
    name: "Father's Day",
    description: 'ลูกค้าสมาชิกสามารถพาคุณพ่อมารับเครื่องดื่มฟรี 1 แก้ว',
    discount: 0.15,
    startDate: '05/12/2024',
    endDate: '06/12/2024',
    status: ['inactive']
  },
  {
    id: 5,
    name: "Children's Day",
    description: 'ลูกค้าสมาชิกที่มีเด็กอายุต่ำกว่า 10 ปี รับของหวานฟรี 1 ชิ้น',
    discount: 0.1,
    startDate: '01/01/2024',
    endDate: '14/01/2024',
    status: ['active']
  },
  {
    id: 6,
    name: 'Discount 30%',
    description: 'ลดราคา 30%',
    discount: 0.3,
    startDate: '01/01/2024',
    endDate: '17/01/2024',
    status: ['active']
  }
]

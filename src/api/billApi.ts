import type { Bill } from "@/views/Bill/types/Bill";

export const defaultBill = <Bill>{
    id: -1,
    date: new Date(),
    worth: 0,
    status: false
}

export const billPaid = <Bill[]>[
    {id: 1, name:'ค่าน้ำ' ,date: new Date('2022-01-01'), worth: 3000, status: true},
    {id: 2, name:'ค่าไฟ' , date: new Date('2022-01-01'), worth: 3100, status: true},
    {id: 3, name:'ค่าน้ำ' , date: new Date('2022-02-01'), worth: 3000, status: true},
    {id: 4, name:'ค่าไฟ' , date: new Date('2022-02-01'), worth: 4000, status: true},
    {id: 5, name:'ค่าน้ำ' , date: new Date('2022-03-01'), worth: 4500, status: true},
    {id: 6, name:'ค่าไฟ' , date: new Date('2022-03-01'), worth: 4700, status: true},
    {id: 7, name:'ค่าน้ำ' , date: new Date('2022-04-01'), worth: 3500, status: true},
    {id: 8, name:'ค่าไฟ' , date: new Date('2022-04-01'), worth: 3800, status: true},
    {id: 9, name:'ค่าน้ำ' , date: new Date('2022-05-01'), worth: 2900, status: true},
    {id: 10, name:'ค่าไฟ' , date: new Date('2022-05-01'), worth: 4500, status: true},
    {id: 11, name:'ค่าน้ำ' , date: new Date('2022-06-01'), worth: 3900, status: true},
    {id: 12, name:'ค่าไฟ' , date: new Date('2022-06-01'), worth: 3800, status: true}
]

export const billPending = <Bill[]> [
    {id: 1, name:'ค่าน้ำ' ,date: new Date('2022-07-01'), worth: 3800, status: false},
    {id: 2, name:'ค่าไฟ' ,date: new Date('2022-07-01'), worth: 3800, status: false},

]
import type { Member } from '@/views/Customer/types/Member'

export const defaultCustomer = {
  id: -1,
  fname: '',
  lname: '',
  tel: '',
  point: 0,
  birthDate: ''
}

export const customers = <Member[]>[
  { id: 1, fName: 'Tony', lName: 'Stark',tel: '0987654321', point: 135, birthDate: '05/09/1975' },
  { id: 2, fName: 'Steve', lName: 'Rogers',tel: '0876543210', point: 89, birthDate: '08/04/1988' },
  { id: 3, fName: 'Thor',lName: 'Odinson', tel: '0456789012', point: 97, birthDate: '09/09/1952' },
  { id: 4, fName: 'Natasha', lName: 'Romanoff', tel: '0123456789', point: 47, birthDate: '03/07/2003' },
  { id: 5, fName: 'Bruce', lName: 'Banner', tel: '0934856721', point: 67, birthDate: '02/02/1990' },
  { id: 6, fName: 'Peter', lName: 'Parker',tel: '0765432109', point: 25, birthDate: '12/10/1967' },
  { id: 7, fName: 'T', lName: 'Challa', tel: '0890123456', point: 54, birthDate: '05/06/1980' },
  { id: 8, fName: 'Peter', lName: 'Quill',tel: '0345678901', point: 34, birthDate: '07/12/1995' },
  { id: 9, fName: 'Carol', lName: 'Danvers', tel: '0567890123', point: 46, birthDate: '04/01/1948' },
  { id: 10, fName: 'Dr.Stephen', lName: 'Strange', tel: '0213456789', point: 76, birthDate: '01/05/2009' },
  { id: 11, fName: 'Bugs', lName: 'Bunny',tel: '0654321098', point: 51, birthDate: '10/03/1972' },
  { id: 12, fName: 'Daffy', lName: 'Duck',tel: '0321098765', point: 23, birthDate: '11/08/1984' },
  { id: 13, fName: 'Porky', lName: 'pig', tel: '0789012345', point: 45, birthDate: '02/01/1960' },
  { id: 14, fName: 'Elmer', lName: 'Fudd', tel: '0543210987', point: 36, birthDate: '09/04/1998' },
  { id: 15, fName: 'Tweety', lName: 'Bird', tel: '0432109876', point: 25, birthDate: '08/06/1957' },
  { id: 16, fName: 'Sylvester', lName: 'the Cat', tel: '0980123456', point: 35, birthDate: '07/09/1982' },
  { id: 17, fName: 'Foghorn', lName: 'Leghorn', tel: '0879012345', point: 75, birthDate: '05/12/1965' },
  { id: 18, fName: 'Yosemite', lName: 'Sam', tel: '0765432108', point: 42, birthDate: '03/02/1978' },
  { id: 19, fName: 'Wile E.', lName: 'Coyote',tel: '0123456780', point: 69, birthDate: '01/10/2000' },
  { id: 20, fName: 'Road', lName: 'Runner',tel: '0897654321', point: 38, birthDate: '04/05/2002' }
]

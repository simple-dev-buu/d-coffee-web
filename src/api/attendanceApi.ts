import type { CheckTime } from '@/views/Checktime/types/checktime'
import { users } from './userApi'

export const defaultChItem = {
  id: -1,
  date: '',
  user: null,
  clockIn: '',
  clockOut: '',
  timeWorked: 0,
  typeWork: null
}

export const attendanceRecord = <CheckTime[]>[
  {
    id: 1,
    date: '1/7/2021',
    user: users[0],
    clockIn: '10:00:00',
    clockOut: '18:00:00',
    timeWorked: 8,
    typeWork: 'Normal'
  },
  {
    id: 2,
    date: '2/3/2021',
    user: users[1],
    clockIn: '10:00:00',
    clockOut: '18:00:00',
    timeWorked: 8,
    typeWork: 'Normal'
  },
  {
    id: 3,
    date: '2/4/2021',
    user: users[2],
    clockIn: '10:00:00',
    clockOut: '19:00:00',
    timeWorked: 9,
    typeWork: 'Normal/OT'
  },
  {
    id: 4,
    date: '2/4/2021',
    user: users[3],
    clockIn: '10:00:00',
    clockOut: '18:00:00',
    timeWorked: 8,
    typeWork: 'Normal'
  },
  {
    id: 5,
    date: '2/5/2021',
    user: users[4],
    clockIn: '10:00:00',
    clockOut: '19:00:00',
    timeWorked: 9,
    typeWork: 'Normal/OT'
  }
]

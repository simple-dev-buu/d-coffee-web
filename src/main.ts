import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

import { fa } from 'vuetify/iconsets/fa'

import '@mdi/font/css/materialdesignicons.css'
// import '@fortawesome/fontawesome-free/css/all.css'

import App from './App.vue'
import router from './router'
import { useAuthStore } from './services/auth'

const app = createApp(App)

const vuetify = createVuetify({
  components,
  directives,
  theme: {
    defaultTheme: 'dark',
    themes: {
      dark: {
        dark: true,
        colors: {
          primary: '#482A1D',
          secondary: '#7f5a3b',
          success: ''
        }
      }
    }
  },
  icons: {
    defaultSet: 'mdi',
    // aliases,
    sets: {
      // mdi,
      fa
    }
  }
})

app.use(createPinia())
app.use(router)
app.use(vuetify)

const authStore = useAuthStore()

router.beforeEach((to, from, next) => {
  // Check if the route requires authentication
  if (to.meta.requiresAuth) {
    // Check if the user is authenticated
    if (!authStore.methods.isAuthenticated()) {
      // Redirect to the login page if not authenticated
      next({ name: 'login' })
    } else {
      // Check if the user is authorized to access the route
      const routeRoles = (to.meta.roles as string[]) || []
      if (!authStore.methods.isAuthorized(routeRoles)) {
        // Redirect to a forbidden or unauthorized page
        next({ name: 'forbidden' })
      } else {
        // Continue to the requested route
        next()
      }
    }
  } else {
    // For routes that don't require authentication, proceed
    next()
  }
})

app.mount('#app')

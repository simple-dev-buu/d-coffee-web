import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/Home/views/HomeView.vue'
import LoginView from '../views/Auth/views/LoginView.vue'
// import ExampleUserVue from '@/components/ExampleUser.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: LoginView
    },
    // {
    //   path: '/example',
    //   name: 'example user',
    //   component: ExampleUserVue
    // },
    {
      path: '/home',
      name: 'home',
      component: HomeView,
      meta: { requiresAuth: true, roles: ['admin'] }
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/About/views/AboutView.vue'),
      meta: { requiresAuth: true, roles: ['admin', 'employee'] }
    },
    {
      path: '/pos',
      name: 'pos',
      component: () => import('../views/POS/view/POSView.vue'),
      meta: { requiresAuth: true, roles: ['admin', 'employee'] }
    },
    {
      path: '/product',
      name: 'product',
      component: () => import('../views/Product/view/ProductView.vue'),
      meta: { requiresAuth: true, roles: ['admin', 'employee'] }
    },
    {
      path: '/customer',
      name: 'customer',
      component: () => import('../views/Customer/views/CustomerView.vue'),
      meta: { requiresAuth: true, roles: ['admin', 'employee'] }
    },
    {
      path: '/promotion',
      name: 'promotion',
      component: () => import('../views/Promotion/view/PromotionView.vue'),
      meta: { requiresAuth: true, roles: ['admin'] }
    },
    {
      path: '/stock',
      name: 'stock',
      component: () => import('../views/Stock/view/StockView.vue'),
      meta: { requiresAuth: true, roles: ['admin', 'employee'] }
    },
    {
      path: '/users',
      name: 'users',
      component: () => import('../views/User/views/UserView.vue'),
      meta: { requiresAuth: true, roles: ['admin'] }
    },
    {
      path: '/employee',
      name: 'employee',
      component: () => import('../views/Employee/views/EmployeeView.vue'),
      meta: { requiresAuth: true, roles: ['admin'] }
    },
    {
      path: '/salary',
      name: 'salary',
      component: () => import('../views/Salary/views/SalaryView.vue'),
      meta: { requiresAuth: true, roles: ['admin'] }
    },
    {
      path: '/receipt',
      name: 'receipt',
      component: () => import('../views/Receipt/views/ReceiptView.vue'),
      meta: { requiresAuth: true, roles: ['admin'] }
    },
    {
      path: '/checktime',
      name: 'checktime',
      component: () => import('../views/Checktime/views/ChecktimeView.vue'),
      meta: { requiresAuth: true, roles: ['admin', 'employee'] }
    },
    {
      path: '/bill',
      name: 'bill',
      component: () => import('../views/Bill/views/BillViews.vue'),
      meta: { requiresAuth: true, roles: ['admin'] }
    }
  ]
})

export default router

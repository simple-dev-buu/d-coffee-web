export const routesWithIcon = new Map([
  ['home', 'mdi-home'],
  ['about', 'mdi-information'],
  ['login', 'mdi-login'],
  ['pos', 'mdi-point-of-sale'],
  ['product', 'mdi-food'],
  ['customer', 'mdi-wallet-membership'],
  ['promotion', 'mdi-star'],
  ['stock', 'mdi-warehouse'],
  ['users', 'mdi-account-multiple'],
  ['employee', 'mdi-account-group-outline'],
  ['salary', 'mdi-currency-usd'],
  ['receipt', 'mdi-note-text'],
  ['checktime', 'mdi-clock']
  
])
